# SolarTask

## Installing dependencies

    yarn

## Running the project in watch mode

    yarn start

## Building the project

    yarn build

LOCAL: http://127.0.0.1:3000/

HOSTED: https://www.omnicrash.net/private/solartask/

## Some facts:
- Developed using VSCode
- Env: Win10 + WSL Debian
- Anti-aliased
- 60FPS+ with 22k+ panels
- Supports touch
- Developed in about ~3 days, learning TS & ReactJS in between

# Day 1 (~6h)
- TS learning
- App skeleton
- ThreeJS base implementation
- Render ground

Result: fancy 'hello world' state

# Day 2 (~6h)
- Building
- Panels
- Panel SRT calculations
- Instancing
- Lighting

Result: App fully functional w/o UI

# Day 3 (~7h)
- ReactJS implementation
- material-ui implementation
- Styling
- Sliders
- Config file
- Linking all parts together
- Testing, debugging, bugfixing
- Polishing
- Deployment

Result: All requirements met, stable 60FPS+ product!