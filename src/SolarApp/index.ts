import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import Stats from 'three/examples/jsm/libs/stats.module';

import World from './world';

const DEBUG: boolean = true;

export interface updateEvent {
	(world: World): void;
}

export class SolarApp {
	private readonly _renderer: THREE.WebGLRenderer;
	private readonly _camera: THREE.PerspectiveCamera;
	private readonly _controls: OrbitControls;
	private readonly _stats: Stats | null = null;

	private readonly _world: World;
	public get world() { return this._world; }

	private _running = false;
	public get running() { return this._running; }

	// Deltas aren't required for this example, but implemented them as a learning exercise & for debugging
	private _lastFrameTimestamp: number = 0;
	public get lastFrameTimestamp() { return this._lastFrameTimestamp; }

	private _delta: number = 0;
	public get delta() { return this._delta; }

	public updateEvent: updateEvent | null = null;

	constructor(canvasElement: HTMLCanvasElement) {
		this._renderer = new THREE.WebGLRenderer({
			canvas: canvasElement,
			antialias: true,
		});
		// 'XNA' Cornflower blue
		this._renderer.setClearColor(new THREE.Color(0x6495ED));
		this._renderer.setPixelRatio(devicePixelRatio);
		this._renderer.setSize(window.innerWidth, window.innerHeight);
		// this._renderer.shadowMap.enabled = true;
		// this._renderer.shadowMap.type = THREE.PCFSoftShadowMap;

		if (DEBUG) {
			// Since stats is only used for debugging, we don't care about managing it with React	
			this._stats = Stats();
			document.body.appendChild(this._stats.dom);
		}

		this._camera = new THREE.PerspectiveCamera(45, innerWidth / innerHeight, 0.1, 1500);
				
		this._camera.position.set(25, 70, 40);

		this._controls = new OrbitControls(this._camera, this._renderer.domElement);
		this._controls.maxPolarAngle = Math.PI * 0.47;
		this._controls.minDistance = 10;
		this._controls.maxDistance = 500;
		this._controls.enablePan = false;

		this._world = new World();

		window.addEventListener('resize', this.onResize, false);

	}

	private onResize = () => {
		this._renderer.setSize(innerWidth, innerHeight);
		this._camera.aspect = innerWidth / innerHeight;
		this._camera.updateProjectionMatrix();
		
		this.render();

	}

	public start() {
		this._running = true;

		this._lastFrameTimestamp = window.performance.now();
		this.mainLoop(this._lastFrameTimestamp);
		
	}

	public stop() {
		this._running = false;

	}

	public update(delta: number = 0) {
		if (this._stats) {
			this._stats.update();
		}
		
		this._controls.update();
		let updated = this._world.update(delta);
		if (!updated) return;

		this._controls.target.y = this._world.building.height;

		if (this.updateEvent) {
			this.updateEvent(this._world)
		};
	}

	public render() {
		this._renderer.render(this._world.scene, this._camera)

	}

	private mainLoop = (timestamp: number = 0) => {
		if (!this._running) return;

		//TODO: Usually I would implement a system to check if the tab is in focus here, to save battery life for the user
		//      Skipped for the purpose if this exercise 

		requestAnimationFrame(this.mainLoop);

		this._delta = timestamp - this._lastFrameTimestamp;
		this._lastFrameTimestamp = timestamp;

		this.update(this._delta);
		this.render();

	}

}

export default SolarApp;