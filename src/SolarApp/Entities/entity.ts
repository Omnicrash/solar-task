import * as THREE from 'three';

export abstract class Entity<Parent extends IEntity | null, Child extends IEntity | null> implements IEntity {
	public parent: Parent | null = null;
	public child: Child | null = null;
	
	// This could be replaced with a flag to only mark certain parts for update, a single boolean is enough for this example
	protected _dirty: boolean = true;
	public get dirty() { return this._dirty; }
	public markDirty() { this._dirty = true; }

	constructor(parent: Parent | null = null) {
		this.parent = parent;
		if (parent) {
			parent.child = this;
		}
		
	}

	public update(delta: number, force: boolean = false): boolean {
		let updated = this._dirty || force;
		updated = this.child?.update(delta, updated) || updated;
		this._dirty = false;
		
		return updated;
	}

}

export interface IEntity {
	parent: IEntity | null;
	child: IEntity | null;

	update(delta: number, force: boolean): boolean;

}

export interface IRenderable extends IEntity {
	readonly scene: THREE.Scene;

}

export default Entity;