import * as THREE from 'three';

import { Entity, IRenderable, Building } from '.';

import * as CONFIG from '../../config';
const PANEL_COUNT_MAX: number = CONFIG.PANEL_ROW_COUNT_MAX * CONFIG.PANEL_COLUMN_COUNT_MAX;

export class PanelArray extends Entity<Building, null> implements IRenderable {
	public readonly scene: THREE.Scene;

	private readonly _panelGeometry: THREE.BoxGeometry;
	private readonly _panelMaterial: THREE.MeshBasicMaterial;
	private readonly _panelMesh: THREE.InstancedMesh;

	private readonly _legGeometry: THREE.BoxGeometry;
	private readonly _legMaterial: THREE.MeshBasicMaterial;
	private readonly _legMesh: THREE.InstancedMesh;
	
	private readonly _panelRefObj = new THREE.Object3D();
	private readonly _legRefObj = new THREE.Object3D();

	private _rows: number = 0;
	public get rows(): number { return this._rows; }
	private _columns: number = 0;
	public get columns(): number { return this._columns; }
	public get count(): number { return this._rows * this._columns; }

	// All of these could be easily outfitted with a public prop, to allow them to be configurable real-time
	private _panelThickness: number = CONFIG.PANEL_THICKNESS;
	private _legThickness: number = CONFIG.PANEL_LEG_THICKNESS;
	private _baseLegLift: number = CONFIG.PANEL_LEG_LIFT;

	private _panelRowSpacing: number = CONFIG.PANEL_ROW_SPACING;
	private _panelColumnSpacing: number = CONFIG.PANEL_COLUMN_SPACING;

	// The amount of space that should be left between all edges of the building and the panels
	private _margin: number = CONFIG.PANEL_EDGE_MARGIN;

	private _panelWidth: number = CONFIG.PANEL_WIDTH_DEFAULT;
	public get panelWidth(): number {
		return this._panelWidth;
	}
	public set panelWidth(val: number) {
		this._panelWidth = THREE.MathUtils.clamp(val, CONFIG.PANEL_WIDTH_MIN, CONFIG.PANEL_WIDTH_MAX);
		this.markDirty();
	}

	private _panelLength: number = CONFIG.PANEL_LENGTH_DEFAULT;
	public get panelLength(): number {
		return this._panelLength;
	}
	public set panelLength(val: number) {
		this._panelLength = THREE.MathUtils.clamp(val, CONFIG.PANEL_LENGTH_MIN, CONFIG.PANEL_LENGTH_MAX);
		this.markDirty();
	}

	private _panelAngle: number = CONFIG.PANEL_ANGLE_DEFAULT;
	public get panelAngle(): number {
		return this._panelAngle;
	}
	public set panelAngle(val: number) {
		this._panelAngle = THREE.MathUtils.clamp(val, CONFIG.PANEL_ANGLE_MIN, CONFIG.PANEL_ANGLE_MAX);
		this.markDirty();
	}

	constructor(building: Building, scene: THREE.Scene) {
		super(building);
		this.scene = scene;

		// PANELS //
		// Using a cube instead of a plane for better visuals
		this._panelGeometry = new THREE.BoxGeometry();
		this._panelMaterial = new THREE.MeshPhongMaterial({ color: 0x000000, shininess: 50 });
		this._panelMesh = new THREE.InstancedMesh(this._panelGeometry, this._panelMaterial, PANEL_COUNT_MAX);
		this._panelMesh.instanceMatrix.setUsage(THREE.DynamicDrawUsage);

		// this._panelMesh.receiveShadow = false;
		// this._panelMesh.castShadow = true;

		// Apply the scale to the reference obj instead of the mesh
		this._panelRefObj.scale.y = this._panelThickness;

		this.scene.add(this._panelMesh);
		
		// LEGS //
		this._legGeometry = new THREE.BoxGeometry();
		this._legMaterial = new THREE.MeshPhongMaterial({ color: 0x000000 });
		this._legMesh = new THREE.InstancedMesh(this._legGeometry, this._legMaterial, PANEL_COUNT_MAX * 4);
		this._panelMesh.instanceMatrix.setUsage(THREE.DynamicDrawUsage);

		// this._panelMesh.receiveShadow = false;
		// this._panelMesh.castShadow = true;

		this._legRefObj.scale.x = this._legRefObj.scale.z = this._legThickness;

		this.scene.add(this._legMesh);

	}

	public update(delta: number, force: boolean = false): boolean {
		// Don't run update logic every frame, only when changes have been flagged
		if (!(this._dirty || force)) return super.update(delta, force);
		
		let roofHeight = this.parent?.height ?? 0;

		// Calculate the virtual triangle under the panel
		//let angleB = 180 - THREE.MathUtils.degToRad(this._angle) - 90;
		let rearAddedHeight = this._panelLength * Math.sin(THREE.MathUtils.degToRad(this._panelAngle));
		let baseLength = Math.sqrt(Math.pow(this._panelLength, 2) - Math.pow(rearAddedHeight, 2));

		this._columns = THREE.MathUtils.clamp(((this.parent?.width ?? 0) - (this._margin * 2)) / (this._panelWidth + this._panelRowSpacing) | 0, 0, CONFIG.PANEL_COLUMN_COUNT_MAX);
		this._rows = THREE.MathUtils.clamp(((this.parent?.depth ?? 0) - (this._margin * 2)) / (baseLength + this._panelColumnSpacing) | 0, 0, CONFIG.PANEL_ROW_COUNT_MAX);

		let panelCount = this._rows * this._columns;
		this._panelMesh.count = panelCount;
		this._legMesh.count = panelCount * 4;

		let buildingOffsetX = (this._columns - 1) * (this._panelWidth + this._panelRowSpacing) * 0.5;
		let buildingOffsetZ = -(this._rows - 1) * (baseLength + this._panelColumnSpacing) * 0.5;

		let i = 0;
		let offsetX = 0;
		let offsetZ = 0;
		for (let z = 0; z < this._rows; z++) {
			for (let x = 0; x < this._columns; x++) {
				offsetX = x * (this._panelWidth + this._panelRowSpacing) - buildingOffsetX;
				offsetZ = -z * (baseLength + this._panelColumnSpacing) - buildingOffsetZ + baseLength * 0.5;

				// Update the panels
				this._panelRefObj.scale.x = this._panelWidth;
				this._panelRefObj.scale.z = this._panelLength;
				this._panelRefObj.rotation.x = THREE.MathUtils.degToRad(this._panelAngle);
				this._panelRefObj.position.x = offsetX;
				this._panelRefObj.position.z = -(baseLength * 0.5) + offsetZ;
				this._panelRefObj.position.y = roofHeight + this._baseLegLift + (rearAddedHeight * 0.5);
				
				this._panelRefObj.updateMatrix();
				this._panelMesh.setMatrixAt(i, this._panelRefObj.matrix);

				// Update the legs
				// Front Left
				this._legRefObj.scale.y = this._legRefObj.scale.y = this._baseLegLift;
				this._legRefObj.position.x = -(this._panelWidth * 0.5) + offsetX;
				this._legRefObj.position.z = this._legRefObj.position.z = offsetZ;
				this._legRefObj.position.y = this._legRefObj.position.y = roofHeight + (this._baseLegLift * 0.5);
				
				this._legRefObj.updateMatrix();
				this._legMesh.setMatrixAt(i * 4, this._legRefObj.matrix);
				
				// Front Right
				this._legRefObj.position.x = this._panelWidth * 0.5 + offsetX;
				
				this._legRefObj.updateMatrix();
				this._legMesh.setMatrixAt((i * 4) + 1, this._legRefObj.matrix);
				
				// Rear Left
				this._legRefObj.scale.y = this._legRefObj.scale.y = this._baseLegLift + rearAddedHeight;
				this._legRefObj.position.x = -(this._panelWidth * 0.5) + offsetX;
				this._legRefObj.position.z = this._legRefObj.position.z = -baseLength + offsetZ;
				this._legRefObj.position.y = this._legRefObj.position.y = roofHeight + ((this._baseLegLift + rearAddedHeight) * 0.5);
				
				this._legRefObj.updateMatrix();
				this._legMesh.setMatrixAt((i * 4) + 2, this._legRefObj.matrix);
				
				// Rear Right
				this._legRefObj.position.x = this._panelWidth * 0.5 + offsetX;
				
				this._legRefObj.updateMatrix();
				this._legMesh.setMatrixAt((i * 4) + 3, this._legRefObj.matrix);
				
				i++;
			}
		}

		this._panelMesh.instanceMatrix.needsUpdate = true;
		this._legMesh.instanceMatrix.needsUpdate = true;

		return super.update(delta, force);
	}

}

export default PanelArray;