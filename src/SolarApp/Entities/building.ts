import * as THREE from 'three';

import {  Entity, IRenderable, Ground, PanelArray } from '.';

import * as CONFIG from '../../config';

export class Building extends Entity<Ground, PanelArray> implements IRenderable {
	public readonly scene: THREE.Scene;

	private readonly _geometry: THREE.BoxGeometry;
	private readonly _material: THREE.MeshBasicMaterial;
	private readonly _mesh: THREE.Mesh;

	private _width: number = CONFIG.BUILDING_WIDTH_DEFAULT;
	public get width(): number {
		return this._width;
	}
	public set width(val: number) {
		val = THREE.MathUtils.clamp(val, CONFIG.BUILDING_WIDTH_MIN, CONFIG.BUILDING_WIDTH_MAX);
		this._width = val;
		this._mesh.scale.x = val;
		this.markDirty();
	}

	private _depth: number = CONFIG.BUILDING_DEPTH_DEFAULT;
	public get depth(): number {
		return this._depth;
	}
	public set depth(val: number) {
		val = THREE.MathUtils.clamp(val, CONFIG.BUILDING_DEPTH_MIN, CONFIG.BUILDING_DEPTH_MAX);
		this._depth = val;
		this._mesh.scale.z = val;
		this.markDirty();
	}

	private _height: number = CONFIG.BUILDING_HEIGHT_DEFAULT;
	public get height(): number {
		return this._height;
	}
	public set height(val: number) {
		val = THREE.MathUtils.clamp(val, CONFIG.BUILDING_HEIGHT_MIN, CONFIG.BUILDING_HEIGHT_MAX);
		this._height = val;
		this._mesh.position.y = (val * 0.5);
		this._mesh.scale.y = val;
		this.markDirty();
	}

	constructor(ground: Ground, scene: THREE.Scene) {
		super(ground);
		this.scene = scene;
		
		this._geometry = new THREE.BoxGeometry();
		this._material = new THREE.MeshPhongMaterial({ color: 0xA85A1A });
		this._mesh = new THREE.Mesh(this._geometry, this._material);

		// Apply initial params
		this.height = this._height;
		this.width = this._width;
		this.depth = this._depth;

		// this._mesh.receiveShadow = true;
		// this._mesh.castShadow = true;

		this.scene.add(this._mesh);

	}

	public update(delta: number, force: boolean = false): boolean {
		
		return super.update(delta, force);
	}

}

export default Building;