import * as THREE from 'three';

import { Entity, IRenderable, Building } from '.';

import * as CONFIG from '../../config';

export class Ground extends Entity<null, Building> implements IRenderable {
	public readonly scene: THREE.Scene;

	private readonly _geometry: THREE.PlaneGeometry;
	private readonly _material: THREE.MeshBasicMaterial;
	private readonly _mesh: THREE.Mesh;

	private _margin: number = CONFIG.GROUND_MARGIN;

	constructor(scene: THREE.Scene) {
		super();
		this.scene = scene;

		this._geometry = new THREE.PlaneGeometry(2, 2);
		this._material = new THREE.MeshPhongMaterial( {color: 0x18AA14 } );
		this._mesh = new THREE.Mesh( this._geometry, this._material );

		this._mesh.rotation.x = THREE.MathUtils.degToRad(-90);

		//this._mesh.receiveShadow = true;

		// Will be positioned at world origin
		this.scene.add(this._mesh);

	}

	public update(delta: number, force: boolean = false): boolean {
		if (this.child?.dirty) {
			this._mesh.scale.x = this.child.width + this._margin;
			this._mesh.scale.y = this.child.depth + this._margin;
		}

		return super.update(delta, force);
	}

}

export default Ground;