import * as THREE from 'three';

import * as Entities from './Entities';

export class World {
	public readonly scene: THREE.Scene;

	// Root obj
	public readonly ground: Entities.Ground;

	// Ref kept here for easy access
	public readonly building: Entities.Building;
	public readonly panelArray: Entities.PanelArray;

	public readonly pointLight: THREE.PointLight;
	public readonly ambientLight: THREE.AmbientLight;

	public lightDistance: number = 10;

	constructor() {
		this.scene = new THREE.Scene();
		
		this.ground = new Entities.Ground(this.scene);
		this.building = new Entities.Building(this.ground, this.scene);
		this.panelArray = new Entities.PanelArray(this.building, this.scene);

		this.pointLight = new THREE.PointLight(0xFFFFFF, 1, 1000);
		this.pointLight.position.x = 10;
		this.pointLight.position.z = 10;
		//this.light.castShadow = true;
		this.scene.add(this.pointLight);

		this.ambientLight = new THREE.AmbientLight(0x6495ED);
		this.scene.add(this.ambientLight);

		//this.scene.fog = new THREE.Fog(0x6495ED);
		//this.scene.fog?.color = new THREE.Color(0x6495ED);

	}

	public update(delta: number = 0): boolean {
		// Update the root obj
		let updated = this.ground.update(delta);

		this.pointLight.position.y = this.building.height + this.lightDistance;

		return updated;
	}

}

export default World;