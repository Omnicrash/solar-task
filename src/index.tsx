import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Slider, Box, Typography } from '@material-ui/core';

import SolarApp from "./SolarApp";
import World from './SolarApp/world';
import * as CONFIG from './config';

const canvas = React.createRef<HTMLCanvasElement>();
let solarApp: SolarApp;

function SolarTask() {
	//TODO: Better TS mappings
	const [buildingWidth, setBuildingWidth] = React.useState(CONFIG.BUILDING_WIDTH_DEFAULT);
	const changeBuildingWidth = (event: any, newValue: number | number[]) => {
		setBuildingWidth(newValue as number);
		solarApp.world.building.width = newValue as number;
	};

	
	const [buildingDepth, setBuildingDepth] = React.useState(CONFIG.BUILDING_DEPTH_DEFAULT);
	const changeBuildingDepth = (event: any, newValue: number | number[]) => {
		setBuildingDepth(newValue as number);
		solarApp.world.building.depth = newValue as number;
	};

	const [buildingHeight, setBuildingHeight] = React.useState(CONFIG.BUILDING_HEIGHT_DEFAULT);
	const changeBuildingHeight = (event: any, newValue: number | number[]) => {
		setBuildingHeight(newValue as number);
		solarApp.world.building.height = newValue as number;
	};

	const [panelAngle, setPanelAngle] = React.useState(CONFIG.PANEL_ANGLE_DEFAULT);
	const changePanelAngle = (event: any, newValue: number | number[]) => {
		setPanelAngle(newValue as number);
		solarApp.world.panelArray.panelAngle = newValue as number;
	};

	const [panelWidth, setPanelWidth] = React.useState(CONFIG.PANEL_WIDTH_DEFAULT);
	const changePanelWidth = (event: any, newValue: number | number[]) => {
		setPanelWidth(newValue as number);
		solarApp.world.panelArray.panelWidth = newValue as number;
	};

	const [panelLength, setPanelLength] = React.useState(CONFIG.PANEL_LENGTH_DEFAULT);
	const changePanelLength = (event: any, newValue: number | number[]) => {
		setPanelLength(newValue as number);
		solarApp.world.panelArray.panelLength = newValue as number;
	};

	const [panelCount, setPanelCount] = React.useState(0);

	React.useEffect(() => {
		if (!canvas.current) {
			console.error("Canvas element not available while mounting renderer");
			return;
		}
		solarApp = new SolarApp(canvas.current);
		solarApp.updateEvent = (world: World) => {
			setPanelCount(world.panelArray.count);
		};
		solarApp.start();

	}, []);
	
	return (
		<div>
			<canvas id="main-canvas" ref={canvas} />
			<div id="panel-border">
				<Box id="panel-count">
					<Typography id="panel-count-label">{panelCount}<br />Panels</Typography>
				</Box>
				<Box id="panel-controls">
					<Typography id="building-width-slider-label" gutterBottom>Building Width</Typography>
					<Slider aria-labelledby="building-width-slider-label"
						onChange={changeBuildingWidth}
						value={buildingWidth}
						min={CONFIG.BUILDING_WIDTH_MIN}
						max={CONFIG.BUILDING_WIDTH_MAX}
						step={CONFIG.BUILDING_WIDTH_STEP}
						valueLabelDisplay='auto'
					/>

					<Typography id="building-depth-slider-label" gutterBottom>Building Depth</Typography>
					<Slider aria-labelledby="building-depth-slider-label"
						onChange={changeBuildingDepth}
						value={buildingDepth} 
						min={CONFIG.BUILDING_DEPTH_MIN}
						max={CONFIG.BUILDING_DEPTH_MAX}
						step={CONFIG.BUILDING_DEPTH_STEP}
						valueLabelDisplay='auto'
					/>

					<Typography id="building-height-slider-label" gutterBottom>Building Height</Typography>
					<Slider aria-labelledby="building-height-slider-label"
						onChange={changeBuildingHeight}
						value={buildingHeight}
						min={CONFIG.BUILDING_HEIGHT_MIN}
						max={CONFIG.BUILDING_HEIGHT_MAX}
						step={CONFIG.BUILDING_HEIGHT_STEP}
						valueLabelDisplay='auto'
					/>

					<Typography id="panel-angle-slider-label" gutterBottom>Panel Angle</Typography>
					<Slider aria-labelledby="panel-angle-slider-label"
						onChange={changePanelAngle}
						value={panelAngle}
						min={CONFIG.PANEL_ANGLE_MIN}
						max={CONFIG.PANEL_ANGLE_MAX}
						step={CONFIG.PANEL_ANGLE_STEP}
						valueLabelDisplay='auto'
					/>

					<Typography id="panel-width-slider-label" gutterBottom>Panel Width</Typography>
					<Slider aria-labelledby="panel-width-slider-label" 
						onChange={changePanelWidth}
						value={panelWidth}
						min={CONFIG.PANEL_WIDTH_MIN}
						max={CONFIG.PANEL_WIDTH_MAX}
						step={CONFIG.PANEL_WIDTH_STEP}
						valueLabelDisplay='auto'
					/>

					<Typography id="panel-length-slider-label" gutterBottom>Panel Length</Typography>
					<Slider aria-labelledby="panel-length-slider-label"
						onChange={changePanelLength}
						value={panelLength}
						min={CONFIG.PANEL_LENGTH_MIN}
						max={CONFIG.PANEL_LENGTH_MAX}
						step={CONFIG.PANEL_LENGTH_STEP}
						valueLabelDisplay='auto'
					/>
				</Box>
			</div>
		</div>
	);

}

ReactDOM.render(<SolarTask />, document.getElementById('root'));