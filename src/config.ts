/// GROUND ///
export const GROUND_MARGIN = 200;


/// BUILDING ///
export const BUILDING_WIDTH_DEFAULT = 30;
export const BUILDING_WIDTH_MIN = 10;
export const BUILDING_WIDTH_MAX = 300;
export const BUILDING_WIDTH_STEP = 0.1;

export const BUILDING_DEPTH_DEFAULT = 15;
export const BUILDING_DEPTH_MIN = 10;
export const BUILDING_DEPTH_MAX = 300;
export const BUILDING_DEPTH_STEP = 0.1;

export const BUILDING_HEIGHT_DEFAULT = 30;
export const BUILDING_HEIGHT_MIN = 10;
export const BUILDING_HEIGHT_MAX = 200;
export const BUILDING_HEIGHT_STEP = 0.1;


/// PANELS ///
export const PANEL_ROW_COUNT_MAX = 150;
export const PANEL_COLUMN_COUNT_MAX = 150;

export const PANEL_ANGLE_DEFAULT = 20;
export const PANEL_ANGLE_MIN = 0;
export const PANEL_ANGLE_MAX = 60;
export const PANEL_ANGLE_STEP = 1;

export const PANEL_WIDTH_DEFAULT = 2;
export const PANEL_WIDTH_MIN = 1;
export const PANEL_WIDTH_MAX = 20;
export const PANEL_WIDTH_STEP = 0.1;

export const PANEL_LENGTH_DEFAULT = 1.2;
export const PANEL_LENGTH_MIN = 1;
export const PANEL_LENGTH_MAX = 10;
export const PANEL_LENGTH_STEP = 0.1;

export const PANEL_THICKNESS = 0.02;
export const PANEL_LEG_THICKNESS = 0.015;
export const PANEL_LEG_LIFT = 0.1;

export const PANEL_ROW_SPACING = 0.3;
export const PANEL_COLUMN_SPACING = 0.6;

// The amount of space between the edge of the building and the panels that should be guaranteed
export const PANEL_EDGE_MARGIN = 0.5;
